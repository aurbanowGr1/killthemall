package web;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import repositories.IRepositoryCatalog;
import domain.Game;

@SessionScoped
@Transactional
@Named("gameBean")
public class GameFormBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private ListDataModel<Game> games = new ListDataModel<Game>();

	private Game game = new Game();

	@Inject
	private IRepositoryCatalog catalog;

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public String saveGame() {
		catalog.getGames().add(game);
		game = new Game();
		return "showGames";
	}

	public ListDataModel<Game> getGames() {

		games.setWrappedData(catalog.getGames().getAll());
		return games;
	}

	public String deleteGame() {
		Game game = games.getRowData();
		Game gameToDelete = catalog.getGames().get(game.getId());
		catalog.getGames().delete(gameToDelete);
		return "showGames";
	}

	public String editGame() {
		game = games.getRowData();

		return "editGame";
	}
	
	public String changeAll() {

		Game g = catalog.getGames().get(game.getId());
		g.setName(game.getName());

		return "showGames";
	}

}
