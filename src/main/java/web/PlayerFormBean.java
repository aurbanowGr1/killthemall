package web;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import repositories.IRepositoryCatalog;
import domain.Player;

@SessionScoped
@Transactional
@Named("playerBean")
public class PlayerFormBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private ListDataModel<Player> players = new ListDataModel<Player>();

	private Player player = new Player();

	@Inject
	private IRepositoryCatalog catalog;

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public String savePlayer() {
		catalog.getPlayers().add(player);
		player = new Player();
		return "showPlayers";
	}

	public ListDataModel<Player> getPlayers() {

		players.setWrappedData(catalog.getPlayers().getAll());
		return players;
	}

	public String deletePlayer() {
		Player player = players.getRowData();
		Player playerToDelete = catalog.getPlayers().get(player.getId());
		catalog.getPlayers().delete(playerToDelete);
		return "showPlayers";
	}

	public String editPlayer() {
		player = players.getRowData();

		return "editPlayer";
	}
	
	public String changeAll() {

		Player p = catalog.getPlayers().get(player.getId());
		p.setNickname(player.getNickname());
		p.setMatches(player.getMatches());
		p.setKillRatio(player.getKillRatio());
		p.setNationality(player.getNationality());
		return "showPlayers";
	}

}
