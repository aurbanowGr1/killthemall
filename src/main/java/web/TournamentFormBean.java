package web;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import repositories.IRepositoryCatalog;
import domain.Tournament;

@SessionScoped
@Transactional
@Named("tournamentBean")
public class TournamentFormBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private ListDataModel<Tournament> tournaments = new ListDataModel<Tournament>();

	private Tournament tournament = new Tournament();

	@Inject
	private IRepositoryCatalog catalog;

	public Tournament getTournament() {
		return tournament;
	}

	public void setTournament(Tournament tournament) {
		this.tournament = tournament;
	}

	public String saveTournament() {
		catalog.getTournaments().add(tournament);
		tournament = new Tournament();
		return "showTournaments";
	}

	public ListDataModel<Tournament> getTournaments() {

		tournaments.setWrappedData(catalog.getTournaments().getAll());
		return tournaments;
	}

	public String deleteTournament() {
		Tournament tournament = tournaments.getRowData();
		Tournament tournamentToDelete = catalog.getTournaments().get(tournament.getId());
		catalog.getTournaments().delete(tournamentToDelete);
		return "showTournaments";
	}

	public String editTournament() {
		tournament = tournaments.getRowData();

		return "editTournament";
	}
	
	public String changeAll() {

		Tournament t = catalog.getTournaments().get(tournament.getId());
		t.setName(tournament.getName());
		t.setDate(tournament.getDate());
		t.setParticipantCount(tournament.getParticipantCount());
		t.setPlace(tournament.getPlace());

		return "showTournaments";
	}

}
