package web;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import repositories.IRepositoryCatalog;
import domain.User;

@SessionScoped
@Transactional
@Named("userBean")
public class UserFormBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private ListDataModel<User> users = new ListDataModel<User>();

	private User user = new User();

	@Inject
	private IRepositoryCatalog catalog;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String saveUser() {
		catalog.getUsers().add(user);
		user = new User();
		return "showUsers";
	}

	public ListDataModel<User> getUsers() {

		users.setWrappedData(catalog.getUsers().getAll());
		return users;
	}

	public String deleteUser() {
		User user = users.getRowData();
		User userToDelete = catalog.getUsers().get(user.getId());
		catalog.getUsers().delete(userToDelete);
		return "showUsers";
	}

	public String editUser() {
		user = users.getRowData();

		return "editUser";
	}

	public String login() {
		List<User> users = catalog.getUsers().getAll();

		for (User u : users) {
			if (u.getLogin().equals(user.getLogin())
					&& u.getPassword().equals(user.getPassword())) {
				saveToSession();
			}
		}

		return "home";
	}

	private void saveToSession() {
		FacesContext fc = FacesContext.getCurrentInstance();

		HttpSession session = (HttpSession) fc.getExternalContext().getSession(
				true);

		if (user.getLogin() != null && !user.getLogin().equals("")) {
			session.setAttribute("user", user);
		}
	}

	public String changePassword() {

		User u = catalog.getUsers().get(user.getId());
		u.setPassword(user.getPassword());

		return null;
	}

	public String changeLogin() {

		User u = catalog.getUsers().get(user.getId());
		u.setLogin(user.getLogin());

		return null;
	}

	public String changeAll() {

		User u = catalog.getUsers().get(user.getId());
		u.setLogin(user.getLogin());
		u.setPassword(user.getPassword());

		return "showUsers";
	}
}
