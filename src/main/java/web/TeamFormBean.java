package web;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import repositories.IRepositoryCatalog;
import domain.Team;

@SessionScoped
@Transactional
@Named("teamBean")
public class TeamFormBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private ListDataModel<Team> teams = new ListDataModel<Team>();

	private Team team = new Team();

	@Inject
	private IRepositoryCatalog catalog;

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public String saveTeam() {
		catalog.getTeams().add(team);
		team = new Team();
		return "showTeams";
	}

	public ListDataModel<Team> getTeams() {

		teams.setWrappedData(catalog.getTeams().getAll());
		return teams;
	}

	public String deleteTeam() {
		Team team = teams.getRowData();
		Team teamToDelete = catalog.getTeams().get(team.getId());
		catalog.getTeams().delete(teamToDelete);
		return null;
	}

	public String editTeam() {
		team = teams.getRowData();

		return "editTeam";
	}
	
	public String changeAll() {

		Team t = catalog.getTeams().get(team.getId());
		t.setName(team.getName());
		t.setMemberCount(team.getMemberCount());
		return "showTeams";
	}

}
