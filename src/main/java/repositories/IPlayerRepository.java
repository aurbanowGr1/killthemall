package repositories;

import domain.Game;
import domain.Player;
import domain.Team;
import domain.Tournament;
import java.util.List;

public interface IPlayerRepository extends IRepository<Player> {
    
    public Player getPlayerByNickname (String nickname);
    public List<Player> getPlayersByMatches (int matches);
    public List<Player> getPlayersByGame (Game game);
    public List<Player> getPlayersByNationality (String nationality);
    public List<Player> getPlayersByTeam (Team team);
    public List<Player> getPlayersByKillRatio (float killRatio);
    public List<Player> getPlayersByTournament (Tournament tournament);
    
}
