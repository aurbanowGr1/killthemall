package repositories.implementations;

import java.util.List;

import javax.persistence.EntityManager;

import repositories.IRepository;
import domain.Game;

public class JpaGameRepository implements IRepository<Game>{

	private EntityManager em;
	
	public JpaGameRepository(EntityManager em) {
		super();
		this.em = em;
	}

	@Override
	public Game get(int id) {
		return em.find(Game.class, id);
	}

	@Override
	public List<Game> getAll() {
		return em.createNamedQuery("game.all", Game.class).getResultList();
	}
	
	@Override
	public void add(Game entity) {
		em.persist(entity);
	}

	@Override
	public void delete(Game entity) {
		em.remove(entity);
	}

	@Override
	public void update(Game entity) {
		
	}

}
