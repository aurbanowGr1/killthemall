package repositories.implementations;

import java.util.List;

import javax.persistence.EntityManager;

import repositories.IRepository;
import domain.Player;

public class JpaPlayerRepository implements IRepository<Player>{

	private EntityManager em;
	
	public JpaPlayerRepository(EntityManager em) {
		super();
		this.em = em;
	}

	@Override
	public Player get(int id) {
		return em.find(Player.class, id);
	}

	@Override
	public List<Player> getAll() {
		return em.createNamedQuery("player.all", Player.class).getResultList();
	}

	@Override
	public void add(Player entity) {
		em.persist(entity);
	}

	@Override
	public void delete(Player entity) {
		em.remove(entity);
	}

	@Override
	public void update(Player entity) {
		
	}

}
