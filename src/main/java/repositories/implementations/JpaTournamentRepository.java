package repositories.implementations;

import java.util.List;

import javax.persistence.EntityManager;

import repositories.IRepository;
import domain.Tournament;

public class JpaTournamentRepository implements IRepository<Tournament>{

	private EntityManager em;
	
	public JpaTournamentRepository(EntityManager em) {
		super();
		this.em = em;
	}

	@Override
	public Tournament get(int id) {
		return em.find(Tournament.class, id);
	}

	@Override
	public List<Tournament> getAll() {
		return em.createNamedQuery("tournament.all", Tournament.class).getResultList();
	}

	@Override
	public void add(Tournament entity) {
		em.persist(entity);
	}

	@Override
	public void delete(Tournament entity) {
		em.remove(entity);
	}

	@Override
	public void update(Tournament entity) {
		
	}

}
