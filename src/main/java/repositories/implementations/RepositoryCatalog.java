package repositories.implementations;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import repositories.IRepository;
import repositories.IRepositoryCatalog;
import domain.Game;
import domain.Player;
import domain.Team;
import domain.Tournament;
import domain.User;

@Stateless
public class RepositoryCatalog implements IRepositoryCatalog {

	@Inject
    private EntityManager em;
	
	@Override
	public IRepository<Game> getGames() {
		return new JpaGameRepository(em);
	}

	@Override
	public IRepository<Player> getPlayers() {
		return new JpaPlayerRepository(em);
	}

	@Override
	public IRepository<Team> getTeams() {
		return new JpaTeamRepository(em);
	}

	@Override
	public IRepository<Tournament> getTournaments() {
		return new JpaTournamentRepository(em);
	}

	@Override
	public IRepository<User> getUsers() {
		return new JpaUserRepository(em);
	}

}
