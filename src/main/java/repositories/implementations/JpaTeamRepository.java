package repositories.implementations;

import java.util.List;

import javax.persistence.EntityManager;

import repositories.IRepository;
import domain.Team;

public class JpaTeamRepository implements IRepository<Team>{

	private EntityManager em;
	
	public JpaTeamRepository(EntityManager em) {
		super();
		this.em = em;
	}

	@Override
	public Team get(int id) {
		return em.find(Team.class, id);
	}

	@Override
	public List<Team> getAll() {
		return em.createNamedQuery("team.all", Team.class).getResultList();
	}

	@Override
	public void add(Team entity) {
		em.persist(entity);
	}

	@Override
	public void delete(Team entity) {
		em.remove(entity);
	}

	@Override
	public void update(Team entity) {
		
	}

}
