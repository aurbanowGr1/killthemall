package repositories;

import domain.User;

public interface IUserRepository extends IRepository<User> {
    
    public User getUserByLogin(String login);
    
}
