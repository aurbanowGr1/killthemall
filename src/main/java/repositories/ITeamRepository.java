package repositories;

import domain.Game;
import domain.Player;
import domain.Team;
import java.util.List;

public interface ITeamRepository extends IRepository<Team> {
    
    public Team getTeamByName (String name);
    public List<Team> getTeamsByGame (Game game);
    public List<Team> getTeamsByPlayer (Player player);
    public List<Team> getTeamsByMemberCount (int memberCount);
    
}
