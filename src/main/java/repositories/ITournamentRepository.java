package repositories;

import domain.Game;
import domain.Player;
import domain.Team;
import domain.Tournament;
import java.util.Date;
import java.util.List;

public interface ITournamentRepository extends IRepository<Tournament> {
    
    public Tournament getTournamentByName (String name);
    public Tournament getTournamentByPlace (String place);
    public List<Tournament> getTournamentsByDate (Date date);
    public List<Tournament> getTournamentsByParticipantCount (int participantCount);
    public List<Tournament> getTournamentsByGame (Game game);
    public List<Tournament> getTournamentsByPlayer (Player player);
    public List<Tournament> getTournamentsByTeam (Team team);
    
}
