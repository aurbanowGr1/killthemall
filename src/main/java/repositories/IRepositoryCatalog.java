package repositories;

import domain.*;

public interface IRepositoryCatalog {

	public IRepository<Game> getGames();
	public IRepository<Player> getPlayers();
	public IRepository<Team> getTeams();
	public IRepository<Tournament> getTournaments();
	public IRepository<User> getUsers();
}