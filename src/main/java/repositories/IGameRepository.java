package repositories;

import domain.Game;
import domain.Player;
import domain.Team;
import domain.Tournament;
import java.util.List;

public interface IGameRepository extends IRepository<Game> {
    
    public Game getGameByName (String name);
    public Game getGameByTournament (Tournament tournament);
    public Game getGameByTeam (Team team);
    public List<Game> getGamesByPlayer (Player player);
    
}
