package domain;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@NamedQuery(name="user.all", query="select u from User u")
public class User extends EntityBase {
		
    private String login;
    private String password;
    
    @OneToOne
    private Player player;
    
    public User(String login, String password) {
        super();
        this.login = login;
        this.password = password;
    }
    
    public User() {
    	
    }
    
    
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}
    
    
    
}
