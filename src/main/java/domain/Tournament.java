package domain;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@NamedQuery(name="tournament.all", query="Select t from Tournament t")
public class Tournament extends EntityBase {
	
	private String name;
    @Temporal(TemporalType.DATE)
	private Date date;
    
    private String place;
    private int participantCount;
    
    @ManyToOne (cascade=CascadeType.ALL)
    @JoinColumn(name="game_id")
    private Game game;
    
    @ManyToMany
    private List<Player> players;
    
    @ManyToMany(mappedBy="tournaments", cascade=CascadeType.PERSIST, fetch=FetchType.LAZY)
    private List<Team> teams;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

     public void setParticipantCount(int participantCount) {
        this.participantCount = participantCount;
    }
    
    public int getParticipantCount() {
        return participantCount;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }
    
}
