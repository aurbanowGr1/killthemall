package domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
@NamedQuery(name="game.all", query="Select g from Game g")

public class Game extends EntityBase {
    
	private String name;
    
	@ManyToMany(mappedBy="games",fetch=FetchType.LAZY, cascade=CascadeType.PERSIST)
	private List<Player>players;
	
	@OneToMany(mappedBy="game",fetch=FetchType.LAZY, cascade=CascadeType.PERSIST)
    private List<Team>teams;
	
	@OneToMany(mappedBy="game",fetch=FetchType.LAZY, cascade=CascadeType.PERSIST)
    private List<Tournament> tournaments;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public List<Tournament> getTournaments() {
        return tournaments;
    }

    public void setTournaments(List<Tournament> tournaments) {
        this.tournaments = tournaments;
    }
    
    
}
