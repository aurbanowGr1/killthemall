package domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@NamedQuery(name="player.all", query="Select p from Player p")
public class Player extends EntityBase {
	
	private String nickname;
    private int matches;
    private float killRatio;
    private String nationality;
    
    @ManyToMany
    private List<Game> games;
    
    @ManyToMany(mappedBy="players", cascade=CascadeType.PERSIST, fetch=FetchType.LAZY)
    private List<Team> teams;
    
    @ManyToMany(mappedBy="players", cascade=CascadeType.PERSIST, fetch=FetchType.LAZY)
    private List<Tournament> tournaments;
    
    @OneToOne(mappedBy="player", cascade=CascadeType.PERSIST, fetch=FetchType.LAZY)
    private User user;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getMatches() {
        return matches;
    }

    public void setMatches(int matches) {
        this.matches = matches;
    }

    public float getKillRatio() {
        return killRatio;
    }

    public void setKillRatio(float killRatio) {
        this.killRatio = killRatio;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public List<Tournament> getTournaments() {
        return tournaments;
    }

    public void setTournaments(List<Tournament> tournaments) {
        this.tournaments = tournaments;
    }
    
}
