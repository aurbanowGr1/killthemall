UserViewModel = function () {
    var self = this;
    self.login = ko.observable();
    self.password = ko.observable();
    
    self.save = function () {
        var data = ko.toJSON(self);
        $.ajax({
            url: "http://localhost:8080/killthemall/resources/users/add",
            type: "POST",
            data: data,
            contentType: "application/json",
            success: function (data) {
                window.location = "http://localhost:8080/killthemall/adduser.xhtml";
            },
            error: function (XMLHttpRequest, testStatus, errorThrown) {
                alert("błąd");
            }
        });
    }
};